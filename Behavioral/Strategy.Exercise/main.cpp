#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <numeric>
#include <fstream>
#include <iterator>
#include <list>
#include <stdexcept>
#include <functional>

struct StatResult
{
	std::string description;
	double value;

	StatResult(const std::string& desc, double val) : description(desc), value(val)
	{
	}
};

using Results = std::vector<StatResult>;

using Statistics = std::function<void (const std::vector<double>&, Results&)>;

struct Avg
{
    void operator()(const std::vector<double>& data, Results& results)
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);
        double avg = sum / data.size();

        StatResult result("AVG", avg);
        results.push_back(result);
    }
};

struct Min
{
    void operator()(const std::vector<double>& data, Results& results)
    {
        double min = *(std::min_element(data.begin(), data.end()));

        results.push_back(StatResult("MIN", min));
    }
};

struct Max
{
    void operator()(const std::vector<double>& data, Results& results)
    {
        double max = *(std::max_element(data.begin(), data.end()));

        results.push_back(StatResult("MAX", max));
    }
};

class StatGroup
{
private:
    std::vector<Statistics> stats_;

public:
    StatGroup() = default;

    StatGroup(std::initializer_list<Statistics> stats)
    {
        stats_.insert(stats_.end(), stats);
    }

    void add(Statistics stat)
    {
        stats_.push_back(stat);
    }

    void operator()(const std::vector<double>& data, Results& results)
    {
        for(const auto& stat : stats_)
            stat(data, results);
    }
};

class DataAnalyzer
{
    Statistics statistics_;
	std::vector<double> data_;
	Results results_;
public:
    DataAnalyzer(Statistics statistics) : statistics_(statistics)
	{
	}

	void load_data(const std::string& file_name)
	{
		data_.clear();
		results_.clear();

		std::ifstream fin(file_name.c_str());
		if (!fin)
			throw std::runtime_error("File not opened");

		double d;
		while (fin >> d)
		{
			data_.push_back(d);
		}

		std::cout << "File " << file_name << " has been loaded...\n";
	}

    void set_statistics(Statistics statistics)
	{
        statistics_ = statistics;
	}

	void calculate()
	{
        statistics_(data_, results_);
	}

	const Results& results() const
	{
		return results_;
	}
};

void show_results(const Results& results)
{
    for(const auto& rslt : results)
		std::cout << rslt.description << " = " << rslt.value << std::endl;
}

int main()
{
    Avg AVG;
    StatGroup MINMAX({ Min(), Max() });
    Statistics SUM = [](const std::vector<double>& data, Results& results ) {
                         double sum = std::accumulate(data.begin(), data.end(), 0.0);
                         results.push_back(StatResult("SUM", sum));
                     };

    StatGroup std_statistics = { AVG, MINMAX, SUM };

    DataAnalyzer da {std_statistics};
	da.load_data("data.dat");
	da.calculate();

	show_results(da.results());

	std::cout << "\n\n";

	da.load_data("new_data.dat");
	da.calculate();

	show_results(da.results());
}
