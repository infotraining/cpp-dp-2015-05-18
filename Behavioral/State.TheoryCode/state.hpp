#ifndef STATE_HPP_
#define STATE_HPP_

#include <iostream>
#include <string>
#include <memory>
#include <typeinfo>


class Context;

// "State"
class State
{
public:
	virtual void handle(Context& context) = 0;
	virtual ~State() {}
};

// "ConcreteStateA"
class ConcreteStateA : public State
{
public:
	void handle(Context& context);
};


// "ConcreteStateB"
class ConcreteStateB : public State
{
public:
	void handle(Context& context);
};


// "Context"
class Context
{
	std::unique_ptr<State> state_;

public:
	Context(std::unique_ptr<State> initial_state) : state_{std::move(initial_state)}
	{
	}

	Context(const Context&) = delete;
	Context& operator=(const Context&) = delete;

	void set_state(std::unique_ptr<State> new_state)
	{
		state_ = std::move(new_state);
	}

	void request()
	{
		state_->handle(*this);
	}
};


void ConcreteStateA::handle(Context& context)
{
	std::cout << "Context works in ConcreteStateA" << std::endl;

	context.set_state(std::unique_ptr<State>{ new ConcreteStateB() });
}

void ConcreteStateB::handle(Context& context)
{
	std::cout << "Context works in ConcreteStateB" << std::endl;

	context.set_state(std::unique_ptr<State>{ new ConcreteStateA() });
}

#endif /*STATE_HPP_*/
