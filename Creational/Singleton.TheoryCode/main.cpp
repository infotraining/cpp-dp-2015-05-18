#include <iostream>
#include "singleton.hpp"

using namespace std;

int main()
{
    Singleton::instance().do_something();

    Meyers::Singleton& singleObject = Meyers::Singleton::instance();
	singleObject.do_something();
}
