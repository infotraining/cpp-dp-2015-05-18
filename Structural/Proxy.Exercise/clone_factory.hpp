#ifndef CLONE_FACTORY_HPP
#define CLONE_FACTORY_HPP

#include <string>
#include <unordered_map>
#include <stdexcept>
#include <memory>
#include "shape.hpp"
#include "singleton_holder.hpp"

namespace Drawing
{

class Factory
{
public:
    typedef std::unordered_map<std::string, std::shared_ptr<Shape> > PrototypesMap;

private:
	PrototypesMap prototypes_;
public:
    Shape* create(const std::string& id)
	{
        try
        {
            auto& prototype = prototypes_.at(id);

            return prototype->clone();
        }
        catch(const std::out_of_range& e)
        {
            throw std::runtime_error("Unkonwn type identifier: " + id);
        }
	}

    bool register_shape(const std::string& shape_id, Shape* prototype)
	{
        return prototypes_.insert(std::make_pair(shape_id, std::shared_ptr<Shape>(prototype))).second;
	}

	bool unregister_shape(const std::string& shape_id)
	{
		return prototypes_.erase(shape_id) != 0;
	}
};

typedef GenericSingleton::SingletonHolder<Factory> ShapeFactory;

}

#endif
