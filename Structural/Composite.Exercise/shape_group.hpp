#ifndef SHAPE_GROUP_HPP_
#define SHAPE_GROUP_HPP_

#include <memory>
#include "shape.hpp"
#include "clone_factory.hpp"

namespace Drawing
{

using ShapePtr = std::shared_ptr<Shape>;

// TO DO: zaimplementowac kompozyt grupuj�cy kszta�ty geometryczne
class ShapeGroup : public Shape
{
    std::vector<ShapePtr> shapes_;

    // Shape interface
public:
    ShapeGroup() = default;

    ShapeGroup(const ShapeGroup& source)
    {
        for(const auto& src_shape : source.shapes_)
            shapes_.emplace_back(src_shape->clone());
    }

    ShapeGroup& operator=(const ShapeGroup& source)
    {
        ShapeGroup temp(source);
        swap(temp);

        return *this;
    }

    ShapeGroup(ShapeGroup&& source) = default;
    ShapeGroup& operator=(ShapeGroup&& source) = default;

    void swap(ShapeGroup& sg)
    {
        shapes_.swap(sg.shapes_);
    }

    void add(ShapePtr shape)
    {
        shapes_.push_back(shape);
    }

    void draw() const
    {
        for(const auto& shp : shapes_)
            shp->draw();
    }

    void move(int dx, int dy)
    {
        for(const auto& shp : shapes_)
            shp->move(dx, dy);
    }

    void read(std::istream &in)
    {
        int count;

        in >> count;

        for(int i = 0; i < count; ++i)
        {
            std::string type_identifier;
            in >> type_identifier;

            Shape* shp = ShapeFactory::instance().create(type_identifier);
            shp->read(in);

            add(ShapePtr(shp));
        }
    }

    void write(std::ostream &out)
    {
        out << "ShapeGroup " << shapes_.size() << std::endl;

        for(const auto& shp : shapes_)
            shp->write(out);
    }

    Shape *clone() const
    {
        return new ShapeGroup(*this);
    }
};

}

#endif /*SHAPE_GROUP_HPP_*/
